import unittest
from unittest.mock import patch
from flask import Flask
from app import app

class TestApp(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    @patch('app.get_redis')  # Mock the get_redis function
    def test_vote_submission(self, mock_get_redis):
        # Mock the return value of get_redis to return a mock Redis object
        mock_redis = mock_get_redis.return_value
        mock_redis.rpush.return_value = None

        # Perform a POST request to submit a vote
        response = self.app.post('/', data={'vote': 'option_a'})
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
